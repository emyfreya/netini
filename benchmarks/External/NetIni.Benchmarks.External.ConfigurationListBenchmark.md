
BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19043.1237 (21H1/May2021Update)
Intel Core i7-7700K CPU 4.20GHz (Kaby Lake), 1 CPU, 8 logical and 4 physical cores
.NET SDK=5.0.401
  [Host]     : .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT
  DefaultJob : .NET 5.0.10 (5.0.1021.41214), X64 RyuJIT


                                 Method |      Mean |     Error |    StdDev |       Min |       Max | Rank | Code Size |  Gen 0 | Allocated |
--------------------------------------- |----------:|----------:|----------:|----------:|----------:|-----:|----------:|-------:|----------:|
                           List<T>.Find |  7.367 ns | 0.0271 ns | 0.0241 ns |  7.324 ns |  7.402 ns |    1 |     285 B |      - |         - |
 IReadOnlyCollection<T>.SingleOrDefault | 49.226 ns | 0.1886 ns | 0.1672 ns | 48.912 ns | 49.433 ns |    2 |     651 B | 0.0095 |      40 B |
