﻿using System;
using System.Linq.Expressions;
using NetIni.Converters;
using NetIni.Internal;

namespace NetIni.Configuration
{
    public class IniTypeConfiguration<T>
        where T : class, new()
    {
        private readonly IniTypeConfiguration _builder;
        
        internal IniTypeConfiguration(IniTypeConfiguration builder)
        {
            _builder = builder;
        }

        public IniTypeConfiguration<T> AddProperty<TProperty>(
            Expression<Func<T, TProperty>> propertyExpression,
            in string? key = null,
            in string? sectionKey = null,
            ValueConverter<TProperty>? converter = null)
        {
            _builder.AddProperty(
                propertyExpression.GetMemberPath(),
                key,
                sectionKey,
                converter);

            return this;
        }
    }
}
