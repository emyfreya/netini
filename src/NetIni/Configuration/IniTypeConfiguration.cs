﻿using System;
using System.Collections.Generic;
using NetIni.Converters;
using NetIni.Internal;

namespace NetIni.Configuration
{
    public class IniTypeConfiguration : ITypeConfiguration
    {
        private static readonly IValueConverter _defaultConverter = new DefaultValueConverter();

        public Type Type { get; }

        public IReadOnlyCollection<IPropertyConfiguration> Configurations => _configurations;

        private readonly List<IPropertyConfiguration> _configurations;

        internal IniTypeConfiguration(Type type)
        {
            Type = type;
            _configurations = new List<IPropertyConfiguration>();
        }
        
        public IniTypeConfiguration AddProperty(
            in string propertyPath, 
            in string? key = null, 
            in string? sectionKey = null, 
            in IValueConverter? converter = null)
        {
            if (key == null && sectionKey == null)
            {
                throw new IniConfigurationException(NetIniStrings.ConfigurationPropertyNeedsKeyOrSection(propertyPath));
            }

            var configuration = new IniPropertyConfiguration(propertyPath, converter ?? _defaultConverter)
            {
                Key = key,
                SectionKey = sectionKey
            };

            _configurations.Add(configuration);

            return this;
        }
    }
}
