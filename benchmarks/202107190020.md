``` ini

BenchmarkDotNet=v0.13.0, OS=Windows 10.0.19041.1110 (2004/May2020Update/20H1)
Intel Core i7-7700K CPU 4.20GHz (Kaby Lake), 1 CPU, 8 logical and 4 physical cores
.NET SDK=5.0.400-preview.21277.10
  [Host]     : .NET 5.0.8 (5.0.821.31504), X64 RyuJIT
  DefaultJob : .NET 5.0.8 (5.0.821.31504), X64 RyuJIT


```
|                     Method |     Mean |   Error |  StdDev |      Min |      Max | Rank | Code Size |   Gen 0 |  Gen 1 | Gen 2 | Allocated |
|--------------------------- |---------:|--------:|--------:|---------:|---------:|-----:|----------:|--------:|-------:|------:|----------:|
|           ReadBasicMinimum | 229.7 μs | 0.70 μs | 0.62 μs | 228.6 μs | 230.9 μs |    1 |      1 KB | 10.9863 | 0.2441 |     - |     45 KB |
|              ReadOptimized | 231.3 μs | 0.85 μs | 0.75 μs | 229.7 μs | 232.3 μs |    1 |      3 KB |  9.7656 | 0.2441 |     - |     40 KB |
| ReadOptimizedStreamDefault | 252.8 μs | 0.65 μs | 0.57 μs | 251.8 μs | 253.6 μs |    2 |      4 KB |  8.7891 |      - |     - |     36 KB |
