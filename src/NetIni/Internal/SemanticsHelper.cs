﻿namespace NetIni.Internal
{
    internal static class SemanticsHelper
    {
        internal static T? NullOrValue<T>(this T inStruct) where T : struct
        {
            return inStruct.Equals(default(T)) ? null : inStruct;
        }
    }
}
