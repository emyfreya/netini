﻿using System;
using System.Diagnostics;
using System.Reflection;
using NetIni.Internal;

namespace NetIni.Converters
{
    /// <summary>
    /// Converts "0" to <c>false</c> or "1" to <c>true</c>.
    /// </summary>
    [DebuggerDisplay("Numeric → Bool")]
    public class NumericBoolConverter : ValueConverter<bool>
    {
        /// <inheritdoc />
        public override bool Convert(PropertyInfo _, in ReadOnlySpan<char> value)
        {
            if (value.Length != 1)
            {
                throw new InvalidOperationException(NetIniStrings.ConverterValueLengthOutOfRange(value, 1));
            }

            return value[0] switch
            {
                '1' => true,
                _ => false
            };
        }

        /// <inheritdoc />
        public override ReadOnlySpan<char> ConvertBack(in bool value)
        {
            return value switch
            {
                true => "1".AsSpan(),
                false => "0".AsSpan()
            };
        }
    }
}
