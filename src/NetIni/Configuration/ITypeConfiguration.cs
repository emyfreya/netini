﻿using System;
using System.Collections.Generic;

namespace NetIni.Configuration
{
    public interface ITypeConfiguration
    {
        Type Type { get; }

        IReadOnlyCollection<IPropertyConfiguration> Configurations { get; }
    }
}
