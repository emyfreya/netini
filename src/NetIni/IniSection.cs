﻿using System.Collections.Generic;
using System.Linq;

namespace NetIni
{
    /// <summary>
    /// Corresponds to an IniSection.
    /// </summary>
    public sealed record IniSection
    {
        public ICollection<IniProperty> Properties { get; }

        public ICollection<string> Comments { get; }

        public string Name { get; }

        public string? Value
        {
            get
            {
                if (Properties.Count == 0)
                {
                    return null;
                }

                if(Properties.Count == 1)
                {
                    return Properties.Select(e => e.Value).Single();
                }

                return string.Join(" ", Properties.Select(e => e.Value));
            }
        }

        public IniSection(string name)
        {
            Name = name;
            Properties = new List<IniProperty>();
            Comments = new List<string>();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Properties)}: {Properties.Count}, {nameof(Comments)}: {Comments.Count}";
        }
    }
}
