﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using NetIni.Configuration;
using NetIni.Internal;
using NetIni.Serialization;

namespace NetIni
{
    public static partial class IniSerializer
    {
        /// <summary>
        /// Deserializes a file.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static async ValueTask<IniDocument> DeserializeFileAsync(string filePath, IniSerializerOptions? options = null)
        {
            options ??= IniSerializerOptions.Default;

            await using FileStream fileStream = DeserializerHelper.GetReadonlyFileStream(filePath, options.BufferSize);
            return await DeserializeAsync(fileStream, options);
        }

        /// <summary>
        /// Deserializes a file.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IniDocument DeserializeFile(in string filePath, IniSerializerOptions? options = null)
        {
            options ??= IniSerializerOptions.Default;

            using FileStream fileStream = DeserializerHelper.GetReadonlyFileStream(filePath, options.BufferSize);
            return Deserialize(fileStream, options);
        }

        /// <summary>
        /// Deserializes a file.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ValueTask<IniDocument> DeserializeAsync(Stream stream, IniSerializerOptions? options = null)
        {
            if (stream is null)
            {
                throw new ArgumentNullException(nameof(stream));
            }
            
            var iniDocument = new IniDocument();

            // Already at the end lmfao.
            if (stream.Position == stream.Length)
            {
                return new ValueTask<IniDocument>(iniDocument);
            }

            options ??= IniSerializerOptions.Default;

            return new ValueTask<IniDocument>(DeserializerHelper.DeserializeAsync(iniDocument, stream, options));
        }

        /// <summary>
        /// Deserializes a file.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IniDocument Deserialize(Stream stream, IniSerializerOptions? options = null)
        {
            if (stream is null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            options ??= IniSerializerOptions.Default;

            var iniDocument = new IniDocument();

            // Already at the end lmfao.
            if (stream.Position == stream.Length)
            {
                return iniDocument;
            }

            IniSection? currentSection = null;

            var reader = new StreamReader(stream, options.Encoding, false);

            string? line;

            while ((line = reader.ReadLine()) != null)
            {
                DeserializerHelper.Build(iniDocument, currentSection, line.AsSpan(), options, out currentSection);
            }

            return iniDocument;
        }

        /// <summary>
        /// Deserialize a file to an object of type <typeparam name="T"></typeparam>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T Deserialize<T>(Stream stream, IConfiguration configuration)
            where T : class, new()
        {
            IniDocument iniDocument = Deserialize(stream, configuration.Options);

            T deserialized = new();
            
            foreach (IniPropertyConfiguration propertyConfiguration in configuration.Get(typeof(T)).Configurations)
            {
                IniProperty? property = iniDocument.FindProperty(e => e.Key == propertyConfiguration.Key);

                propertyConfiguration.SetProperty(
                    property?.Value ?? ReadOnlySpan<char>.Empty, 
                    deserialized);
            }

            return deserialized;
        }

        /// <summary>
        /// Deserializes a string.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static IniDocument Deserialize(in ReadOnlySpan<char> fileContent, IniSerializerOptions? options = null)
        {
            options ??= IniSerializerOptions.Default;

            int length = fileContent.Length;
            var iniDocument = new IniDocument();

            IniSection? currentSection = null;
            ReadOnlySpan<char> newLineSpan = options.NewLine.AsSpan();

            for (int currentIndex = 0; currentIndex < fileContent.Length;)
            {
                ReadOnlySpan<char> currentView = fileContent[currentIndex..length];
                int nextIndex = currentView.IndexOf(newLineSpan, StringComparison.Ordinal);

                // -1: End of file or missing NewLine at end of file.
                nextIndex = nextIndex == -1 ? fileContent.Length - currentIndex : nextIndex;

                ReadOnlySpan<char> view = currentView[..nextIndex];
                currentIndex += nextIndex + newLineSpan.Length;

                DeserializerHelper.Build(iniDocument, currentSection, in view, options, out currentSection);
            }

            return iniDocument;
        }
    }
}
