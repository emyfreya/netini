﻿using System;
using System.Reflection;

namespace NetIni.Converters
{
    public abstract class ValueConverter<T> : IValueConverter
    {
        public abstract T? Convert(PropertyInfo propertyInfo, in ReadOnlySpan<char> value);
        
        public abstract ReadOnlySpan<char> ConvertBack(in T? value);

        /// <inheritdoc />
        ReadOnlySpan<char> IValueConverter.ConvertBack(in object? value)
        {
            return ConvertBack((T?)value);
        }

        /// <inheritdoc />
        object? IValueConverter.Convert(PropertyInfo propertyInfo, in ReadOnlySpan<char> value)
        {
            return Convert(propertyInfo, in value);
        }
    }
}
