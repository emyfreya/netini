﻿using System;
using System.Linq;
using FluentAssertions;
using NetIni.Configuration;
using NetIni.Converters;
using Xunit;

namespace NetIni.Tests.Configuration
{
    public sealed class IniConfigurationBuilderTest
    {
        private class MyClass
        {
            public bool TopProperty { get; set; }

            public ChildMyClass? Child { get; set; }

            public ChildMyClass ReadOnlyChild { get; }

            public MyClass()
            {
                ReadOnlyChild = new ChildMyClass();
            }
        }

        private class ChildMyClass
        {
            public int ChildProperty { get; set; }
        }

        [Fact]
        public void BuildEmptyConfiguration()
        {
            var builder = new IniConfigurationBuilder();

            IConfiguration build = builder.Build();

            build.Should().NotBeNull();
            build.Options.Should().BeNull();
        }

        [Fact]
        public void GetConfigurationTypeThrowsNotFound()
        {
            IConfiguration configuration = new IniConfigurationBuilder().Build();

            Func<ITypeConfiguration> action = () => configuration.Get(typeof(MyClass));

            action.Should().Throw<IniConfigurationException>();
        }

        [Fact]
        public void MapOneGenericPropertyWithOnlyAKey()
        {
            var builder = new IniConfigurationBuilder();
            
            builder.Map<MyClass>()
                .AddProperty(
                    e => e.Child!.ChildProperty,
                    key: "JoJo",
                    sectionKey: null,
                    new NumericIntConverter());

            IConfiguration build = builder.Build();

            build.Should().NotBeNull();

            Func<ITypeConfiguration> action = () => build.Get(typeof(MyClass));

            action.Should().NotThrow();
            
            ITypeConfiguration result = action();
            result.Should().NotBeNull();
            result.Configurations.Should().NotBeEmpty();
            result.Configurations.Single().Key.Should().Be("JoJo");
            result.Configurations.Single().SectionKey.Should().BeNull();
        }
    }
}
