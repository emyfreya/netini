﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using NetIni.Serialization;

namespace NetIni
{
    public static partial class IniSerializer
    {
        /// <summary>
        /// Serializes the document into the specified <paramref name="filePath"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(IniDocument document, in string filePath, IniSerializerOptions? options = null)
        {
            options ??= IniSerializerOptions.Default;

            using FileStream fileStream = SerializerHelper.GetWriteFileStream(filePath, options.BufferSize);
            Serialize(document, fileStream, options);
        }

        /// <summary>
        /// Serializes the document into the specified <paramref name="stream"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void Serialize(IniDocument document, Stream stream, IniSerializerOptions? options = null)
        {
            options ??= IniSerializerOptions.Default;
            
            Span<byte> bytes = SerializerHelper.GetBytes(document, options.Encoding, options).Span;
            
            stream.Write(bytes);
        }

        /// <summary>
        /// Serializes the document into the specified <paramref name="filePath"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static async ValueTask SerializeAsync(IniDocument document, string filePath, IniSerializerOptions? options = null, CancellationToken cancellationToken = default)
        {
            options ??= IniSerializerOptions.Default;

            await using FileStream fileStream = SerializerHelper.GetWriteFileStream(filePath, options.BufferSize);
            await SerializeAsync(document, fileStream, options, cancellationToken);
        }

        /// <summary>
        /// Serializes the document into the specified <paramref name="stream"/>.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static async ValueTask SerializeAsync(IniDocument document, Stream stream, IniSerializerOptions? options = null, CancellationToken cancellationToken = default)
        {
            options ??= IniSerializerOptions.Default;
            
            ReadOnlyMemory<byte> memoryBytes = SerializerHelper.GetBytes(document, options.Encoding, options);

            await stream.WriteAsync(memoryBytes, cancellationToken);
        }

        /// <summary>
        /// Serializes the document into a string.
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static string Serialize(IniDocument document, IniSerializerOptions? options = null)
        {
            options ??= IniSerializerOptions.Default;

            return document.GetStringBuilder(options);
        }
    }
}
