﻿using System;
using System.IO;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace NetIni.Tests.Serialization
{
    [Collection(nameof(IniSerializerFixture))]
    public class IniSerializerTests : IDisposable
    {
        private string DocumentPath { get; }

        private readonly IniSerializerFixture _fixture;

        public IniSerializerTests(IniSerializerFixture fixture)
        {
            _fixture = fixture;
            DocumentPath = $"my_document_{Guid.NewGuid()}.ini";
        }

        /// <inheritdoc />
        public void Dispose()
        {
            File.Delete(DocumentPath);
        }

        [Fact]
        public void WriteIniDocumentToString()
        {
            string serialized = IniSerializer.Serialize(_fixture.SerializationWithSectionDocument, IniSerializerOptions.Default);

            _fixture.SerializationWithSection.Should().Be(serialized);
        }

        [Fact]
        public void WriteIniDocumentToStream()
        {
            using var fileStream = new FileStream(DocumentPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);

            IniSerializer.Serialize(_fixture.SerializationWithSectionDocument, fileStream, IniSerializerOptions.Default);

            _fixture.SerializationWithSection.Should().Be(fileStream);
        }

        [Fact]
        public async Task WriteIniDocumentToStreamAsync()
        {
            await using var fileStream = new FileStream(DocumentPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);

            await IniSerializer.SerializeAsync(_fixture.SerializationWithSectionDocument, fileStream, IniSerializerOptions.Default);

            _fixture.SerializationWithSection.Should().Be(fileStream);
        }

        [Fact]
        public void WriteIniDocumentToFile()
        {
            IniSerializer.Serialize(_fixture.SerializationWithSectionDocument, DocumentPath, IniSerializerOptions.Default);

            using var fileStream = new FileStream(DocumentPath, FileMode.Open);
            _fixture.SerializationWithSection.Should().Be(fileStream);
        }

        [Fact]
        public async Task WriteIniDocumentToFileAsync()
        {
            await IniSerializer.SerializeAsync(_fixture.SerializationWithSectionDocument, DocumentPath, IniSerializerOptions.Default);

            await using var fileStream = new FileStream(DocumentPath, FileMode.Open);
            _fixture.SerializationWithSection.Should().Be(fileStream);
        }
    }
}
