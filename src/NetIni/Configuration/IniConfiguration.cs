﻿using System;
using System.Collections.Generic;
using NetIni.Internal;

namespace NetIni.Configuration
{
    internal sealed class IniConfiguration : IConfiguration
    {
        public IniSerializerOptions? Options { get; }

        private readonly List<ITypeConfiguration> _configurations;

        internal IniConfiguration(List<ITypeConfiguration> configurations, Action<IniSerializerOptions>? configureOptions)
        {
            _configurations = configurations;

            if (configureOptions != null)
            {
                Options = new IniSerializerOptions();
                configureOptions(Options);
            }
        }
        
        public ITypeConfiguration Get(Type type)
        {
            ITypeConfiguration? configuration = _configurations.Find(e => e.Type == type);

            if (configuration == null)
            {
                throw new IniConfigurationException(NetIniStrings.ConfigurationNotFound(type.FullName!));
            }

            return configuration;
        }
    }
}
