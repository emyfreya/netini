﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;

namespace NetIni.Converters
{
    /// <summary>
    /// Converts a numeric <c>string</c> representation to an <c>int</c>.
    /// </summary>
    [DebuggerDisplay("Numeric → Int")]
    public class NumericIntConverter : ValueConverter<int>
    {
        /// <inheritdoc />
        public override int Convert(PropertyInfo propertyInfo, in ReadOnlySpan<char> value)
        {
            if (value.Length != 1)
            {
                return 0;
            }

            return (int)char.GetNumericValue(value[0]);
        }

        /// <inheritdoc />
        public override ReadOnlySpan<char> ConvertBack(in int value)
        {
            return value.ToString(CultureInfo.InvariantCulture).AsSpan();
        }
    }
}
