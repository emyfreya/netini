﻿using System;
using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;
using NetIni.Internal;

namespace NetIni.Benchmarks.External
{
    public class LinqAnyVsCount
    {
        private static readonly Random Random = new(1698);
        private readonly ICollection<int> _bigCollection;

        public LinqAnyVsCount()
        {
            _bigCollection = System.Linq.Enumerable.Repeat(0, 100000)
                .Select(_ => Random.Next())
                .ToArray();
        }

        [Benchmark]
        public bool LinqAny()
        {
            return _bigCollection.Any();
        }

        [Benchmark]
        public bool LinqAnyNegated()
        {
            return !_bigCollection.Any();
        }

        [Benchmark]
        public bool CollectionCount()
        {
            return _bigCollection.NAny();
        }

        [Benchmark]
        public bool CollectionCountNegated()
        {
            return !_bigCollection.NAny();
        }
    }
}
