﻿using System.IO;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;

namespace NetIni.Benchmarks
{
    public class SerializerReadBenchmark
    {
        private static readonly string _filePath = Path.Combine(Directory.GetCurrentDirectory(), "LightFile.ini");
        private readonly string _fileContent;

        public SerializerReadBenchmark()
        {
            _fileContent = File.ReadAllText(_filePath);
        }

        [Benchmark]
        public IniDocument ReadFromString()
        {
            return IniSerializer.Deserialize(_fileContent);
        }

        [Benchmark]
        public IniDocument ReadFromStringWithoutComments()
        {
            return IniSerializer.Deserialize(_fileContent, new IniSerializerOptions
            {
                IncludeComments = false
            });
        }

        [Benchmark]
        public IniDocument ReadFromStringWithoutSections()
        {
            return IniSerializer.Deserialize(_fileContent, new IniSerializerOptions
            {
                IncludeSections = false
            });
        }

        [Benchmark]
        public IniDocument ReadFromStringWithoutSectionsAndComments()
        {
            return IniSerializer.Deserialize(_fileContent, new IniSerializerOptions
            {
                IncludeSections = false,
                IncludeComments = false
            });
        }

        [Benchmark]
        public IniDocument ReadFromFileStream()
        {
            return IniSerializer.DeserializeFile(_filePath, IniSerializerOptions.Default);
        }

        [Benchmark]
        public ValueTask<IniDocument> ReadFromFileStreamAsync()
        {
            return IniSerializer.DeserializeFileAsync(_filePath, IniSerializerOptions.Default);
        }
    }
}
