﻿using System;
using System.Resources;

namespace NetIni.Internal
{
    public static class NetIniStrings
    {
        private static readonly ResourceManager _resourceManager
            = new("NetIni.Properties.NetIniStrings", typeof(NetIniStrings).Assembly);

        private static string GetString(in string name, params string[] formatterNames)
        {
            string? value = _resourceManager.GetString(name)!;

            for (int i = 0; i < formatterNames.Length; i++)
            {
                value = value.Replace("{" + formatterNames[i] + "}", "{" + i + "}");
            }

            return value;
        }

        public static string ConfigurationNotUnique(Type type)
        {
            return string.Format(
                GetString("ConfigurationNotUnique", "typeName"),
                type.FullName!);
        }

        public static string ConverterValueLengthOutOfRange(in ReadOnlySpan<char> value, in int exactLength)
        {
            return string.Format(
                GetString("ConverterValueLengthOutOfRange", nameof(value), "currentLength", nameof(exactLength)),
                value.ToString(), value.Length, exactLength);
        }

        public static string SerializationPropertyNotFound(in string propertyPath, in string documentTypeFullName)
        {
            return string.Format(
                GetString("SerializationPropertyNotFound", nameof(propertyPath), nameof(documentTypeFullName)),
                propertyPath, documentTypeFullName);
        }

        public static string ConfigurationNotFound(string typeFullName)
        {
            return string.Format(
                GetString("ConfigurationNotFound", nameof(typeFullName)),
                typeFullName);
        }

        public static string ConfigurationPropertyNeedsKeyOrSection(in string propertyPath)
        {
            return string.Format(
                GetString("ConfigurationPropertyNeedsKeyOrSection", nameof(propertyPath)),
                propertyPath);
        }
    }
}
