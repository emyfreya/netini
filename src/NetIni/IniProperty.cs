﻿using System;
using System.Text;

namespace NetIni
{
    public readonly struct IniProperty : IEquatable<IniProperty>
    {
        public string? Key { get; }

        public string Value { get; }

        /// <summary>
        /// Gets the Key / Value as shown in the file.
        /// <para></para>
        /// e.g. 'myKey=value'
        /// </summary>
        public string KeyValue
        {
            get
            {
                var stringBuilder = new StringBuilder();

                if (Key != null)
                {
                    stringBuilder.Append(Key)
                        .Append(IniParserConsts.KeyValueSeparator);
                }

                return stringBuilder.Append(Value).ToString();
            }
        }

        public IniProperty(string key, string value) : this(value)
        {
            Key = key;
        }

        public IniProperty(string value)
        {
            Key = null;
            Value = value;
        }
        
        /// <inheritdoc />
        public override string ToString()
        {
            return $"{nameof(Key)}: {Key}, {nameof(Value)}: {Value}";
        }

        /// <inheritdoc />
        public bool Equals(IniProperty other)
        {
            return Key == other.Key && Value == other.Value;
        }

        /// <inheritdoc />
        public override bool Equals(object? obj)
        {
            return obj is IniProperty other && Equals(other);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return HashCode.Combine(Key, Value);
        }

        public static bool operator ==(IniProperty left, IniProperty right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IniProperty left, IniProperty right)
        {
            return !left.Equals(right);
        }
    }
}
