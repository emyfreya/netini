﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NetIni.Configuration
{
    public sealed class IniConfigurationBuilder
    {
        private readonly SortedDictionary<string, IniTypeConfiguration> _typeConfigurations;
        private Action<IniSerializerOptions>? _configureOptions;

        public IniConfigurationBuilder()
        {
            _typeConfigurations = new SortedDictionary<string, IniTypeConfiguration>();
        }

        public IniConfigurationBuilder Configure(Action<IniSerializerOptions> options)
        {
            _configureOptions = options;

            return this;
        }

        public IniTypeConfiguration Map(Type type)
        {
            if (_typeConfigurations.ContainsKey(GetKey(type)))
            {
                return _typeConfigurations[GetKey(type)];
            }

            var configuration = new IniTypeConfiguration(type);
            
            _typeConfigurations.Add(GetKey(type), configuration);

            return configuration;
        }

        public IniTypeConfiguration<T> Map<T>()
            where T : class, new()
        {
            return new(Map(typeof(T)));
        }

        public IConfiguration Build()
        {
            return new IniConfiguration(_typeConfigurations.Select(e => e.Value).ToList<ITypeConfiguration>(), _configureOptions);
        }

        private static string GetKey(Type type)
        {
            return type.FullName!;
        }
    }
}
