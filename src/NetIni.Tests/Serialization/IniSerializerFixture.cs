﻿using System.IO;
using Xunit;

namespace NetIni.Tests.Serialization
{
    [CollectionDefinition(nameof(IniSerializerFixture))]
    public class IniSerializerFixture : ICollectionFixture<IniSerializerFixture>
    {
        public string SerializationWithSection { get; }

        public IniDocument SerializationWithSectionDocument { get; }

        public IniSerializerFixture()
        {
            using var file = GetFileStream("SerializationWithSection.ini");
            var streamReader = new StreamReader(file);
            SerializationWithSection = streamReader.ReadToEnd();

            SerializationWithSectionDocument = new IniDocument
            {
                Sections =
                {
                    new IniSection("DISPLAY_SETTINGS")
                    {
                        Comments =
                        {
                            "ResolutionXXX => 0 for autodetection",
                            "RefreshRate => 0 will let DirectX pick it",
                            "WindowMode => 0 fullscreen / 1 windowed / 2 borderless",
                            "AspectRatio => 0 display / 1 resolution / 2..N as options",
                            "VSync => 0 disabled / 1 frame / 2 frames",
                            "UseLetterbox => 0 disabled / 1 enabled",
                            "DefaultFOV => Default vertical field of view (degrees)",
                            "InitialWindowPositionX/Y => -1 for centering"
                        },
                        Properties =
                        {
                            new IniProperty("GPUAdapter", "0"),
                            new IniProperty("Monitor", "0"),
                            new IniProperty("ResolutionWidth", "1280"),
                            new IniProperty("ResolutionHeight", "720"),
                            new IniProperty("RefreshRate", "60.000000"),
                            new IniProperty("WindowMode", "1"),
                            new IniProperty("AspectRatio", "0"),
                            new IniProperty("VSync", "0"),
                            new IniProperty("MaxGPUBufferedFrame", "1"),
                            new IniProperty("UseLetterbox", "0"),
                            new IniProperty("DefaultFOV", "85.000000"),
                            new IniProperty("EnableAMDMultiDraw", "1"),
                            new IniProperty("InitialWindowPositionX", "43"),
                            new IniProperty("InitialWindowPositionY", "30")
                        }
                    },
                    new IniSection("HARDWARE_INFO")
                    {
                        Properties =
                        {
                            new IniProperty("GPUVendor", "AMD"),
                            new IniProperty("GPUDeviceId", "0x6811"),
                            new IniProperty("GPUSubSysId", "0x30501462"),
                            new IniProperty("GPUDedicatedMemoryMB", "0x30501462"),
                        }
                    }
                }
            };
        }
        
        private static FileStream GetFileStream(string fileName)
        {
            return TestHelper.GetFileStream(Path.Combine(Directory.GetCurrentDirectory(), "Serialization", "Files", fileName));
        }
    }
}
