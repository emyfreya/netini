﻿namespace NetIni
{
    internal static class IniParserConsts
    {
        public const char SectionNameStart = '[';
        public const char SectionNameEnd = ']';
        public const char KeyValueSeparator = '=';
    }
}
