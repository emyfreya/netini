﻿using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;
using NetIni.Configuration;

namespace NetIni.Benchmarks.External
{
    /// <summary>
    /// Needed to optimize type search on configuration.
    /// <para></para>
    /// Obvious choice is .Find
    /// </summary>
    public class ConfigurationListBenchmark
    {
        private readonly IReadOnlyCollection<IniTypeConfiguration> _configurationReadOnly;
        private readonly List<IniTypeConfiguration> _configurationsList;

        public ConfigurationListBenchmark()
        {
            var configurations = new List<IniTypeConfiguration>
            {
                new(typeof(TestClass1)), 
                new(typeof(TestClass2)),
                new(typeof(TestClass3))
            };

            _configurationReadOnly = configurations.AsReadOnly();
            _configurationsList = configurations;
        }

        private class TestClass1
        {
        }

        private class TestClass2
        {
        }
        private class TestClass3
        {
        }

        [Benchmark(Description = "IReadOnlyCollection<T>.SingleOrDefault")]
        public IniTypeConfiguration SingleOrDefault()
        {
            return _configurationReadOnly.SingleOrDefault(e => e.Type == typeof(TestClass2));
        }

        [Benchmark(Description = "List<T>.Find")]
        public IniTypeConfiguration Find()
        {
            return _configurationsList.Find(e => e.Type == typeof(TestClass2));
        }
    }
}
