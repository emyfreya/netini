﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace NetIni.Internal
{
    internal static class ReflectionHelper
    {
        internal static string GetMemberName<T>(this Expression<T> expression) =>
            expression.Body switch
            {
                MemberExpression m =>
                    m.Member.Name,
                UnaryExpression { Operand: MemberExpression m } =>
                    m.Member.Name,
                _ =>
                    throw new NotImplementedException(expression.GetType().ToString())
            };

        internal static string GetMemberPath<T>(this Expression<T> expression)
        {
            return string.Join('.', expression.GetMemberNameList());
        }

        internal static IReadOnlyList<string> GetMemberNameList<T>(this Expression<T> expression)
        {
            var memberNames = new List<string>();

            GetMemberName(memberNames, expression.Body);

            memberNames.Reverse();

            return memberNames.AsReadOnly();
        }

        private static void GetMemberName(ICollection<string> memberNames, Expression? expression)
        {
            if (expression is MemberExpression m)
            {
                memberNames.Add(m.Member.Name);

                GetMemberName(memberNames, m.Expression);
            }
        }
    }
}
