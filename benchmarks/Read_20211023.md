``` ini

BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19043.1288 (21H1/May2021Update)
Intel Core i7-7700K CPU 4.20GHz (Kaby Lake), 1 CPU, 8 logical and 4 physical cores
.NET SDK=6.0.100-rc.2.21505.57
  [Host]     : .NET 5.0.11 (5.0.1121.47308), X64 RyuJIT
  DefaultJob : .NET 5.0.11 (5.0.1121.47308), X64 RyuJIT


```
|                                   Method |       Mean |     Error |    StdDev |        Min |        Max | Rank | Code Size |   Gen 0 |  Gen 1 | Allocated |
|----------------------------------------- |-----------:|----------:|----------:|-----------:|-----------:|-----:|----------:|--------:|-------:|----------:|
|            ReadFromStringWithoutSections |   5.372 μs | 0.0599 μs | 0.0588 μs |   5.283 μs |   5.457 μs |    1 |      2 KB |  2.7924 | 0.0076 |     11 KB |
|            ReadFromStringWithoutComments |   5.425 μs | 0.1000 μs | 0.1111 μs |   5.326 μs |   5.791 μs |    1 |      2 KB |  2.7924 | 0.0076 |     11 KB |
| ReadFromStringWithoutSectionsAndComments |   5.639 μs | 0.1115 μs | 0.2278 μs |   5.330 μs |   6.254 μs |    2 |      2 KB |  2.7924 | 0.0076 |     11 KB |
|                           ReadFromString |   7.413 μs | 0.1054 μs | 0.0823 μs |   7.211 μs |   7.508 μs |    3 |      3 KB |  4.0970 | 0.0076 |     17 KB |
|                       ReadFromFileStream | 212.192 μs | 2.6361 μs | 2.0581 μs | 209.671 μs | 217.226 μs |    4 |      1 KB |  8.3008 |      - |     34 KB |
|                  ReadFromFileStreamAsync | 296.488 μs | 5.7677 μs | 6.8660 μs | 286.642 μs | 308.776 μs |    5 |      1 KB | 11.7188 |      - |     47 KB |
