﻿using System;
using System.Reflection;

namespace NetIni.Converters
{
    public interface IValueConverter
    {
        object? Convert(PropertyInfo propertyInfo, in ReadOnlySpan<char> value);

        ReadOnlySpan<char> ConvertBack(in object? value);
    }
}
