﻿using System;
using System.Linq.Expressions;
using FluentAssertions;
using NetIni.Internal;
using Xunit;

namespace NetIni.Tests.Internal
{
    public class ReflectionHelperTests
    {
        private class MyClass
        {
            public ChildMyClass? Child { get; set; }

            public bool TopProperty { get; set; }
        }

        private class ChildMyClass
        {
            public bool ChildProperty { get; set; }
        }

        [Fact]
        public void GetTopMemberName()
        {
            Expression<Func<MyClass, bool>> expression = c => c.TopProperty;

            expression.GetMemberPath().Should().Be("TopProperty");
        }

        [Fact]
        public void GetTopAndChildMemberName()
        {
            Expression<Func<MyClass, bool>> expression = c => c.Child!.ChildProperty;

            expression.GetMemberPath().Should().Be("Child.ChildProperty");
        }
    }
}
