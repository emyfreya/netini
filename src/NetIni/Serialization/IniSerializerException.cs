﻿using System;

namespace NetIni.Serialization
{
    public class IniSerializerException : Exception
    {
        /// <inheritdoc />
        public IniSerializerException(string? message) : base(message)
        {
        }

        /// <inheritdoc />
        public IniSerializerException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
