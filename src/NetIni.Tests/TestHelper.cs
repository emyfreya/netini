﻿using System.IO;
using FluentAssertions.Primitives;

namespace NetIni.Tests
{
    internal static class TestHelper
    {
        public static FileStream GetFileStream(string filePath)
        {
            return new(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite, IniSerializerOptions.Default.BufferSize);
        }

        public static void Be(this StringAssertions assertions, FileStream fileStream)
        {
            fileStream.Position = 0;
            var streamReader = new StreamReader(fileStream);

            assertions.Be(streamReader.ReadToEnd());
        }
    }
}
