﻿using System;
using NetIni.Converters;

namespace NetIni.Configuration
{
    public interface IPropertyConfiguration
    {
        string? SectionKey { get; set; }

        string? Key { get; set; }

        string PropertyPath { get; }

        IValueConverter Converter { get; }

        void SetProperty<TDocument>(in ReadOnlySpan<char> value, in TDocument document);
    }
}
