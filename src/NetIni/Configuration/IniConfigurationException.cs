﻿using System;

namespace NetIni.Configuration
{
    public class IniConfigurationException : Exception
    {
        /// <inheritdoc />
        public IniConfigurationException(string? message) : base(message)
        {
        }

        /// <inheritdoc />
        public IniConfigurationException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
