﻿using System;
using System.Reflection;
using System.Text;
using NetIni.Converters;
using NetIni.Internal;
using NetIni.Serialization;

namespace NetIni.Configuration
{
    public sealed class IniPropertyConfiguration : IPropertyConfiguration
    {
        public string? SectionKey { get; set; }

        public string? Key { get; set; }

        public string PropertyPath { get; }

        public IValueConverter Converter { get; }

        internal IniPropertyConfiguration(string propertyPath, IValueConverter converter)
        {
            PropertyPath = propertyPath ?? throw new ArgumentNullException(nameof(propertyPath));
            Converter = converter ?? throw new ArgumentNullException(nameof(converter));
        }

        public void SetProperty<TDocument>(in ReadOnlySpan<char> value, in TDocument document)
        {
            if (document is null)
            {
                throw new ArgumentNullException(nameof(document));
            }

            PropertyInfo? property = null;
            Type currentType = document.GetType();
            object currentValue = document;

            foreach (string s in PropertyPath.Split('.'))
            {
                property = currentType.GetProperty(s);

                if (property == null)
                {
                    break;
                }

                currentType = property.PropertyType;
                object? propertyValue = property.GetValue(currentValue);

                if (property.PropertyType.IsClass)
                {
                    currentValue = propertyValue ?? CreateInstance(property, document);
                }
            }

            if (property == null)
            {
                throw new IniSerializerException(NetIniStrings.SerializationPropertyNotFound(PropertyPath, document.GetType().FullName!));
            }

            property.SetValue(currentValue, Converter.Convert(property, value));
        }

        /// <inheritdoc />
        public override string ToString()
        {
            StringBuilder stringBuilder = new();
            
            if (SectionKey != null)
            {
                stringBuilder.Append(nameof(SectionKey))
                    .Append('=')
                    .AppendLine(SectionKey);
            }

            if (Key != null)
            {
                stringBuilder.Append(nameof(Key))
                    .Append('=')
                    .AppendLine(Key);
            }

            return string.Join(", ", stringBuilder.ToString());
        }

        private static object CreateInstance<TDocument>(PropertyInfo property, in TDocument document)
        {
            object instance = Activator.CreateInstance(property.PropertyType)!;
            property.SetValue(document, instance);

            return instance;
        }
    }
}
