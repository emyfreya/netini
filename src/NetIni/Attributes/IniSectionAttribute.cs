﻿using System;

namespace NetIni.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class IniSectionAttribute : Attribute
    {
        public string Name { get; }

        /// <inheritdoc />
        public IniSectionAttribute(string name)
        {
            Name = name;
        }
    }
}
